#! /bin/bash

docker exec -it mongodb mongodump --db projectedb --out ./projectedb_`date +"%d-%m-%y"` 

docker cp mongodb:/projectedb_`date +"%d-%m-%y"` /var/backups

